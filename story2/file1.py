import logging

logging.basicConfig(filename='file1.log', level=logging.INFO,
                    format='%(levelname)s: %(name)s: %(message)s')

logging.info('Message from file1')
