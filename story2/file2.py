import logging
import file1

logging.basicConfig(filename='file2.log', level=logging.DEBUG,
                    format='%(asctime)s:%(name)s:%(message)s')

logging.debug('Message from file2')
